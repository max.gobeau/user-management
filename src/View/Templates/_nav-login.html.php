<?php if(! $this->isConnected): ?>
    <form class="form-inline mt-2 mt-md-0" method="post">
        <div class="form-group mb-2">
            <input class="form-control" type="text" id="login" name="login" placeholder="Login" aria-label="Login">
        </div>
        <div class="form-group mx-sm-3 mb-2">
            <input class="form-control" type="password" name="password" placeholder="Password" aria-label="Password">
        </div>
        <button class="btn btn-outline-success mb-2" type="submit" formaction="<?= $this->path; ?>/Connect">Connect</button>
    </form>
<?php else: ?>
    <form class="form-inline mt-2 mt-md-0" method="post">
        <div class="form-group mx-sm-3 mb-2">
            <input readonly class="form-control-plaintext text-white" type="text" name="login" placeholder="Login" aria-label="Login" value="<?= $this->user->login; ?>">
        </div>
        <button class="btn btn-outline-success mb-2" type="submit" formaction="<?= $this->path; ?>/Disconnect">Disconnect</button>
    </form>
<?php endif; ?>
