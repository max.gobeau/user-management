-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 16 mai 2019 à 10:15
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `dwwm_session`
--

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

DROP TABLE IF EXISTS `groupe`;
CREATE TABLE IF NOT EXISTS `groupe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `groupe`
--

INSERT INTO `groupe` (`id`, `nom`) VALUES
(1, 'Administrateur(s)'),
(2, 'Comptable(s)'),
(3, 'Technicien(s)'),
(4, 'Secrétaire(s)'),
(5, 'Directeur(s)');

-- --------------------------------------------------------

--
-- Structure de la table `groupe_privilege`
--

DROP TABLE IF EXISTS `groupe_privilege`;
CREATE TABLE IF NOT EXISTS `groupe_privilege` (
  `id_groupe` int(11) NOT NULL,
  `id_privilege` int(11) NOT NULL,
  PRIMARY KEY (`id_groupe`,`id_privilege`),
  KEY `id_privilege` (`id_privilege`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `groupe_privilege`
--

INSERT INTO `groupe_privilege` (`id_groupe`, `id_privilege`) VALUES
(1, 1),
(5, 1),
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(1, 3),
(1, 4),
(1, 5),
(5, 5),
(1, 6),
(2, 6),
(3, 6),
(4, 6),
(5, 6),
(1, 7),
(1, 8),
(1, 9),
(2, 9),
(3, 9),
(4, 9),
(5, 9),
(1, 10),
(1, 11),
(2, 11),
(3, 11),
(4, 11),
(5, 11),
(1, 12),
(1, 13),
(1, 14),
(2, 14),
(3, 14),
(4, 14),
(5, 14),
(1, 15);

-- --------------------------------------------------------

--
-- Structure de la table `privilege`
--

DROP TABLE IF EXISTS `privilege`;
CREATE TABLE IF NOT EXISTS `privilege` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `privilege`
--

INSERT INTO `privilege` (`id`, `nom`) VALUES
(1, 'utilisateur/create'),
(2, 'utilisateur/read'),
(3, 'utilisateur/update'),
(4, 'utilisateur/delete'),
(5, 'groupe/create'),
(6, 'groupe/read'),
(7, 'groupe/update'),
(8, 'groupe/delete'),
(9, 'privilege/read'),
(10, 'affectation/create'),
(11, 'affectation/read'),
(12, 'affectation/delete'),
(13, 'attribution/create'),
(14, 'attribution/read'),
(15, 'attribution/delete');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `login`, `password`) VALUES
(1, 'user', 'ee11cbb19052e40b07aac0ca060c23ee'),
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3'),
(3, 'account', 'e268443e43d93dab7ebef303bbe9642f'),
(4, 'tech', 'd9f9133fb120cd6096870bc2b496805b'),
(5, 'secr', 'a9e81478b786081e124e59a7c4993fdb'),
(6, 'dir', '736007832d2167baaae763fd3a3f3cf1'),
(7, 'techdir', '3ed57dcbdf511157e74b0ede0d174afe');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur_groupe`
--

DROP TABLE IF EXISTS `utilisateur_groupe`;
CREATE TABLE IF NOT EXISTS `utilisateur_groupe` (
  `id_utilisateur` int(11) NOT NULL,
  `id_groupe` int(11) NOT NULL,
  PRIMARY KEY (`id_utilisateur`,`id_groupe`),
  KEY `id_groupe` (`id_groupe`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur_groupe`
--

INSERT INTO `utilisateur_groupe` (`id_utilisateur`, `id_groupe`) VALUES
(2, 1),
(3, 2),
(4, 3),
(7, 3),
(5, 4),
(6, 5),
(7, 5);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `groupe_privilege`
--
ALTER TABLE `groupe_privilege`
  ADD CONSTRAINT `groupe_privilege_ibfk_1` FOREIGN KEY (`id_groupe`) REFERENCES `groupe` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `groupe_privilege_ibfk_2` FOREIGN KEY (`id_privilege`) REFERENCES `privilege` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `utilisateur_groupe`
--
ALTER TABLE `utilisateur_groupe`
  ADD CONSTRAINT `utilisateur_groupe_ibfk_1` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `utilisateur_groupe_ibfk_2` FOREIGN KEY (`id_groupe`) REFERENCES `groupe` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
